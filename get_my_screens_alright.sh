#!/bin/bash

xrandr --output eDP-1-1 --primary

while true; do 

    # check if it needs to be left alone

    DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
    cd $DIR 
    if [ "$( cat leave_me_alone.txt | grep yes )" = "yes" ]; then
        echo "--------- Leaving it alone! --------"
        sleep 20
        continue
    fi

    # get names and states

    PRIMARY_SCREEN=$(xrandr | grep " primary " | awk '{ print$1 }')
    if [[ ! $( xrandr --listactivemonitors | grep $PRIMARY_SCREEN ) = "" ]]; then
        PRIMARY_SCREEN_ACTIVE="YES"
    else
        PRIMARY_SCREEN_ACTIVE="NO"
    fi

    NUM_OF_CONNECTED_MONITORS=$(xrandr | grep " connected " | wc -l)

    if [ $NUM_OF_CONNECTED_MONITORS = 2 ]; then
        SECONDARY_SCREEN=$(xrandr | grep " connected " | grep -v " primary " | awk '{ print$1 }')
        if [[ ! $( xrandr --listactivemonitors | grep $SECONDARY_SCREEN ) = "" ]]; then
            SECONDARY_SCREEN_ACTIVE="YES"
        else
            SECONDARY_SCREEN_ACTIVE="NO"
        fi
    fi

    LID_STATE=$(cat /proc/acpi/button/lid/LID/state | awk '{ print$2 }')

    # Logic
    # case 1: no external screen connected, lid open, primary screen inactive => activate primary screen
    # case 2: no external screen connected, lid closed, primary screen active => deactivate primary screen
    # case 3: external screen connected, external screen inactive => activate external screen, 
    # deactivate primary screen if active

    if [ $NUM_OF_CONNECTED_MONITORS = 1 ]; then # means that no external screen is connected
        if [[ $LID_STATE = "open" ]] && [[ $PRIMARY_SCREEN_ACTIVE = "NO" ]]; then # case 1
            xrandr --output $PRIMARY_SCREEN --auto
            xrandr --auto
        elif [[ $LID_STATE = "closed" ]] && [[ $PRIMARY_SCREEN_ACTIVE = "YES" ]]; then # case 2
            xrandr --output $PRIMARY_SCREEN --off
        fi
    fi

    if [ $NUM_OF_CONNECTED_MONITORS = 2 ]; then # means that one external screen is connected
        if [[ $SECONDARY_SCREEN_ACTIVE = "NO" ]]; then # case 3
            xrandr --output $SECONDARY_SCREEN --auto
            xrandr --auto
        fi
        if [[ $PRIMARY_SCREEN_ACTIVE = "YES" ]]; then # I want the primary screen to be off when an external screen is connected
            xrandr --output $PRIMARY_SCREEN --off
        fi
    fi

    sleep 5

done
