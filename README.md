# Get my Screens Alright

Simple bash script which does some xrandr action when an external screen gets plugged in/out to your laptop

# Disclaimer

* Tested under Ubuntu 18.04 with i3 tiling window manager.
* In ``/etc/systemd/logind.conf`` I've set ``HandleLidSwitch=ignore``. Reload this change via ``systemctl restart systemd-logind``.
* This script is subject to be configured to your personal setup by yourself if you want it to be robust. 
* You will need to autostart the script in the background for it to make sense at all. E.g. in i3 you could place ``exec /usr/bin/env bash [path_to_repo]/get-my-screens-alright/get_my_screens_alright.sh`` in ``~/.config/i3`` which will automatically start the script when you log in to i3.
* You need to place a file called leave_me_alone.txt in the same directory as the script is in. Simply fill it with ``yes`` or ``no``. If the file says yes, the script won't do anything (useful when you're working with a beamer and want your primary screen to stay active).
* This script is implemented quick and dirty. Its nature is polling and therefore it is not resource friendly. Shouldn't take too much of you systems resources though.

## Motivation

I'm using a window manager (i3) which meant I needed to do some manual steps via xrandr/arandr every time I've changed my monitor setup.
It was time to script this.

## What's the script good for?

If you're using one or multiple laptops at a workplace with one external screen and you do not want the laptop standing around with its lid open all the time.

## Will it work with a different setup?

You can change the scripts logic and vars to your desire.

## What's the logic?

Case 1: no external screen connected, lid open, primary screen inactive => activate primary screen
Case 2: no external screen connected, lid closed, primary screen active => deactivate primary screen
Case 3: external screen connected, external screen inactive => activate external screen, deactivate primary screen if active

## Known issues/limitations
* I've experienced that the sound output might hick up every other xrandr command. Consequently this script will mess with your sound every 5 seconds which is obviously not cool. So if you're listening to music, I do not recommend running this script in the background. I'd rather use it as a one shot.
* If your Notebook Built-In monitor happens to be not the monitor marked as ``primary`` the script will probably fail or not do what it is supposed to be. In this case, set the appropriate screen to primary via ``xrandr --output [screen] --primary``.
